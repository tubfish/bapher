# Application entry point script
import sys

from PyQt4 import QtGui

from GUI.MainWindow import ApplicationWindow

qApp = QtGui.QApplication(sys.argv)

app = ApplicationWindow()
app.show()

sys.exit(qApp.exec_())
