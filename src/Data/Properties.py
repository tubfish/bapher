from PyQt4.QtCore import QObject
from PyQt4.QtCore import pyqtSignal
from PyQt4.QtCore import pyqtSlot
from PyQt4.QtCore import QString

class CurrentProperties(QObject):
    """Class to hold current properties of the plot.
    slots can be registered to receive updates"""
    updateSignal = pyqtSignal('QString',name="propertyUpdated")
    def __init__(self):
        super(CurrentProperties,self).__init__()
        self.setDefaultProperties()
    
    def registerForUpdates(self, slot):
        self.updateSignal.connect(slot)
    
    def unregisterForUpdates(self, slot):
        self.updateSignal.disconnect(slot)
    
    def updated(self, updatedProperty):
        self.updateSignal.emit(QString.fromAscii(updatedProperty))
    
    def update(self, prop, value):
        if prop in self.properties:
            self.properties[prop] = value
            self.updated(prop)
        else:
            raise(ValueError("Property not known: "+prop+", "+str(value)))
                
    def setDefaultProperties(self):
        self.properties = { "x_title" : "",     # x axis title
                            "y_title" : "",     # y axis title
                            "x_min"   : 0.0,    # x axis minimum value
                            "x_max"   : 1.0,    # x axis maximum value
                            "y_min"   : 0.0,    # y axis minimum value
                            "y_max"   : 1.0,    # y axis maximum value
                            "x_log"   : False,  # is the x axis plotted on a log scale?
                            "y_log"   : False   # is the y axis plotted on a log scale?
                           }
        
class ApplicationProperties(object):
    """Class to hold application level properties"""
    def __init__(self):
        pass
    
# Function to allow for standalone testing
def main(args):
    properties = CurrentProperties()
    print properties
    @pyqtSlot(str)
    def slt(prop):
        print "Property updated, slt:",prop
        
    def slt2(prop):
        print "Property updated, slt2:",prop
    
    properties.registerForUpdates(slt)
    properties.registerForUpdates(slt2)
    properties.update("x_title","new title")
    properties.unregisterForUpdates(slt)
    properties.update("y_title","new title")

if __name__ == "__main__":
    import sys
    main(sys.argv)