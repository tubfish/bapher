# Table widget
import sys

from PyQt4.QtCore import Qt
from PyQt4.QtGui import QApplication
from PyQt4.QtGui import QKeyEvent
from PyQt4.QtGui import QTableWidget
from PyQt4.QtGui import QTableWidgetItem
       
class MyTable(QTableWidget):
    def __init__(self, data = [], *args):
        QTableWidget.__init__(self, *args)
        self.setData(data)
        
        self.xVals = []
        self.yVals = []
        self.xColour = Qt.green
        self.yColour = Qt.yellow
        self.defaultColour = Qt.white
    
    def setData(self, data):
        self.data = data
        self.setmydata()
            
    def setmydata(self):
        if self.data == []:
            return
        self.clear()
        
        rows = len(self.data)-1
        cols = len(self.data[0].split(","))
        for row in xrange(0,rows,1):
            self.insertRow(row)
        
        for col in xrange(0,cols,1):
            self.insertColumn(col)
            
        horHeaders = self.data[0].replace("\n","").replace("\r","").split(",")
        print horHeaders
        for n in xrange(0,len(horHeaders),1):            
            for m in xrange(1,len(self.data),1):
                item = self.data[m].replace("\n","").replace("\r","").split(",")[n]
                newitem = QTableWidgetItem(item)
                self.setItem(m-1, n, newitem)
        self.setHorizontalHeaderLabels(horHeaders)
        
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
    
    def keyPressEvent(self, event):
        if type(event) == QKeyEvent and event.key() == Qt.Key_X:
            self.getCols("x")
        elif type(event) == QKeyEvent and event.key() == Qt.Key_Y:
            self.getCols("y")
    
    def getCols(self,axis):
        if axis == "x":
            vals = self.xVals
            colour = self.xColour
        elif axis == "y":
            vals = self.yVals
            colour = self.yColour
        else:
            print "Axis not known"
            return
        
        # reset colours
        if len(vals) > 0:
            self.setColours(vals,self.defaultColour)
        
        vals = self.selectedItems()
        self.setColours(vals,colour)
        
        if axis == "x":
            self.xVals = vals
        elif axis == "y":
            self.yVals = vals
            
    def setColours(self, elementList, colour):
        for element in elementList:
            element.setBackground(colour)

# Function to allow for standalone testing
def main(args):
    app = QApplication(args)
    fh = open("../../Data/test.csv","r")
    data= fh.readlines()
    table = MyTable(data)
    table.show()
    sys.exit(app.exec_())
 
if __name__=="__main__":
    main(sys.argv)
