# Main application window
import sys

from PyQt4 import QtGui, QtCore

from FilePreviewTable import MyTable
from GUI.Plotting import PlotWidget

class ApplicationWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("application main window")

        self.file_menu = QtGui.QMenu('&File', self)
        self.file_menu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtGui.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)

        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtGui.QWidget(self)

        l = QtGui.QHBoxLayout(self.main_widget)
        
        self.plotWidget = PlotWidget(self.main_widget, width=5, height=4, dpi=100)
        self.tableWidget = MyTable()
                                   
        l.addWidget(self.tableWidget)
        l.addWidget(self.plotWidget)

        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.statusBar().showMessage("All hail matplotlib!", 2000)

    def setTableFile(self, filepath):
        """Set file to open in preview window"""
        fh = open(filepath,"r")
        data = fh.readlines()
        self.tableWidget.setData(data)
        
    def setPlotXY(self, xList, yList):
        """Set the x and y values for the plot"""
        self.plotWidget.setX(xList)
        self.plotWidget.setY(yList)
        self.plotWidget.drawFigure()
        
    def fileQuit(self):
        """Quit the application"""
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()

    def about(self):
        QtGui.QMessageBox.about(self, "About",
"""Bapher - The Batch Grapher
copyright 2015 James Nally

Batch graphing tool. Written in Python using PyQt and Matplotlib.

Original Matplotlib canvas/application modified from 
http://matplotlib.org/examples/user_interfaces/embedding_in_qt4.html 
by Florent Rougon and Darren Dale"""
)

# Function to allow for standalone testing
def main(args):
    qApp = QtGui.QApplication(sys.argv)
    app = ApplicationWindow()
    
    app.setPlotXY([1,2,3,4,5,6,7,8,9,10], [1,4,9,16,25,36,49,64,81,100])
    app.setTableFile("../../Data/test.csv")
    app.show()
    sys.exit(qApp.exec_())

if __name__=="__main__":
    main(sys.argv)
    