# Plotting widget
import sys

from PyQt4 import QtGui

from matplotlib import rc as mplrc
mplrc("figure", facecolor="white") # change face colour of the plot from grey 

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

class PlotWidget(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        self.xList = []
        self.yList = []
        # We want the axes cleared every time plot() is called
        self.axes.hold(False)
        self.drawFigure()

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,QtGui.QSizePolicy.Expanding,QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
    
    def setX(self,xList):
        """Set the x values to plot"""
        self.xList = xList
    
    def setY(self, yList):
        """Set the y values to plot"""
        self.yList = yList
            
    def drawFigure(self):
        """Draw the figure (update)"""
        self.axes.plot(self.xList, self.yList)

# Function to allow for standalone testing
def main(args):
    app = QtGui.QApplication(args)
    canvas = PlotWidget()
    canvas.setX([1,2,3,4,5,6,7,8,9,10])
    canvas.setY([1,4,9,16,25,36,49,64,81,100])
    canvas.drawFigure()
    canvas.show()
    sys.exit(app.exec_())

if __name__=="__main__":
    main(sys.argv)